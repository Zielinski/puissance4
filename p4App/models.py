from .app import db
from p4 import Grille

class Grille(db.Model):
	id		= db.Column(db.Integer,	primary_key=True)
	yaml	= db.Column(db.Text)
	tour	= db.Column(db.Integer)
	points	= db.Column(db.Integer)
	
	def __repr__(self):
		from yaml import load
		return str(Grille(load(self.yaml))) + ", points: " + str(self.points)
