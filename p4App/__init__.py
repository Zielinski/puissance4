from .app import app, manager, db
import p4App.views
import p4App.commands

from p4 import Grille
import jsonpickle
def Grille(grille_de_session):
	return jsonpickle.decode(grille_de_session)

app.jinja_env.globals.update(Grille=Grille)
