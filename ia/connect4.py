# -*- coding: utf-8 -*-
from p4 import Grille, Joueur, Jeton
from ia import Listenable

class Connect4(Grille, Listenable):
	
	def __init__(self, player1=None, player2=None):
		super(Connect4,self).__init__()
		self.initListenable()
		self.joueur1 = player1 if player1 is not None else Joueur("PC 1")
		self.joueur2 = player2 if player2 is not None else Joueur("PC 2")
	
	def currentPlayer(self):
		return self.joueur1 if self.nbTour%2==0 else self.joueur2
	
	def possibleMoves(self):
		return self.freeIColumns()
	
	def isPossible(self,move):
		return self.grille[0][move]==None
	
	def play(self, move, notify=True):
		if not self.isDone():
			player = self.currentPlayer()
			super(Connect4,self).jouer(Jeton(joueur=player, col=move))
			if notify:
				self.notify(move=move)
	
	def isDone(self):
		return self.estRemplie() or self.puissance4()
	
	def playerHasWon(self, player=None):
		if player is None:
			player = self.currentPlayer()
		if player == self.joueur1:
			return self.j1Gagne
		return self.j2Gagne
	
	def playerHasLost(self, player=None):
		if player is None:
			player = self.currentPlayer()
		if player == self.joueur1:
			return self.j2Gagne
		return self.j1Gagne
	
	def copy(self):
		from copy import deepcopy
		_listenersTmp = self.listeners
		self.listeners = list()
		fake=deepcopy(self)
		self.listeners = _listenersTmp
		return fake
