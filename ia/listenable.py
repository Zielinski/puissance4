# -*- coding: utf-8 -*-
class Listenable:
	
	def __init__(self):
		self.listeners = list()
	
	def initListenable(self):
		self.listeners = list()
	
	def addListener(self,listener):
		self.listeners.append(listener)
	
	def removeListener(self,listener):
		self.listeners.remove(listener)
	
	def notify(self, **kw):
		for listener in self.listeners:
			listener.notify(kw)
